package TP2;

import java.util.ArrayList;

public class StockMarket {
    private ArrayList<Observer> obsrvs = new ArrayList<>();
    private float prix;

    public StockMarket(float prix){
       
        this.prix = prix ;
    }
    
    public StockMarket() {
    }

    


    public ArrayList<Observer> getObsrvs() {
        return obsrvs;
    }

    public void setObsrvs(ArrayList<Observer> obsrvs) {
        this.obsrvs = obsrvs;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    void attach(Observer obsrv){
      obsrvs.add(obsrv);
    }

    void detach(Observer observer){
        obsrvs.remove(observer);
    }
    
    public void notifyObservers(){
        for(Observer obs : obsrvs){
            obs.update();
        }
    }
}

