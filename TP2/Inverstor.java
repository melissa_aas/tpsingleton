package TP2;

public class Inverstor implements Observer {
    private Stock stock;

    
    
    public Inverstor(Stock stock) {
        this.stock = stock;
        this.stock.attach(this);
    }



    public void update(){
        System.out.println("Changement effectué"+stock.getPrix());
    }



    public Stock getStock() {
        return stock;
    }



    public void setStock(Stock stock) {
        this.stock = stock;
    }
}
