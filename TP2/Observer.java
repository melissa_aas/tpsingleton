package TP2;

public interface Observer {

    public void update();
}