package TP2;

import java.util.ArrayList;

public class Stock {
    private ArrayList<Observer> obsrvs = new ArrayList<>();
    private float prix;

    public Stock(float prix){
       
        this.prix = prix ;
    }
    
    public Stock() {
    }

    


    public ArrayList<Observer> getObsrvs() {
        return obsrvs;
    }

    public void setObsrvs(ArrayList<Observer> obsrvs) {
        this.obsrvs = obsrvs;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    void attach(Observer obsrv){
        obsrvs.add(obsrv);
    }
    
}



