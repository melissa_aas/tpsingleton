package TP1;
public class ArbreBinaire {
	
	private Integer clef;
	private ArbreBinaire gauche;
	private ArbreBinaire droite;
	
	public ArbreBinaire() {
		this.clef= null;
		this.droite = null;
		this.gauche = null;
	}

	public Integer getClef() {
		return clef;
	}

	public void setClef(Integer clef) {
		this.clef = clef;
	}

	public ArbreBinaire getGauche() {
		return gauche;
	}

	public void setGauche(ArbreBinaire gauche) {
		this.gauche = gauche;
	}

	public ArbreBinaire getDroite() {
		return droite;
	}

	public void setDroite(ArbreBinaire droite) {
		this.droite = droite;
	}
	
	//3eme
	private static ArbreBinaire arbVide = new ArbreBinaire();
	
	public boolean abVide() {
		boolean result = false;
		if (this.gauche == null && this.droite == null) {
			result = true;
		}
		return result;
	}
	
	//public static ArbreBinaire creer() {
        //return arbVide;
    //}
	
	public void inserer(Integer valeur) {
	    if (this.abVide()) {
	        this.clef = valeur;
	        this.gauche = new ArbreBinaire();
	        this.droite = new ArbreBinaire();
	    } else {
	        if (valeur < this.clef) {
	            this.gauche.inserer(valeur);
	        } else {
	            this.droite.inserer(valeur);
	        }
	    }
	}
	
	public boolean rechercher(Integer valeur) {
        if (this.abVide()) {
            return false;
        } else if (this.clef.equals(valeur)) {
            return true;
        } else if (valeur < this.clef) {
            return this.gauche.rechercher(valeur);
        } else {
            return this.droite.rechercher(valeur);
        }
    }
	
	public int taille() {
        if (this.abVide()) {
            return 0;
        } else {
            return 1 + this.gauche.taille() + this.droite.taille();
        }
    }
}
