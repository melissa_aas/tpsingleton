package TP1;
public class Main {

	public static void main(String[] args) {
		//hello new commit
		ArbreBinaire arbre1 = new ArbreBinaire();
		
		arbre1.inserer(5);
        arbre1.inserer(3);
        arbre1.inserer(8);
        arbre1.inserer(1);
        arbre1.inserer(4);
        arbre1.inserer(7);
        arbre1.inserer(9);
        
        System.out.println("Arbre binaire après insertion :");
        afficherArbre(arbre1);
        System.out.println("Taille de l'arbre : " + arbre1.taille());
        System.out.println("L'arbre est vide : " + arbre1.abVide());
       
        
        System.out.println("Recherche de la valeur 7 : " + arbre1.rechercher(7));
        System.out.println("Recherche de la valeur 2 : " + arbre1.rechercher(2));
        
        
       
	}
	
	 public static void afficherArbre(ArbreBinaire arbre) {
         if (!arbre.abVide()) {
             afficherArbre(arbre.getGauche());
             System.out.print(arbre.getClef() + " ");
             afficherArbre(arbre.getDroite());
         }
     }
}

